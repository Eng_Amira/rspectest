require 'rails_helper'

describe Agent, '#favorite_gadget' do
 
  it 'returns one item, the favorite gadget of the agent ' do
  # Setup
    agent = Agent.create(name: 'James Bond')
    q = Quartermaster.create(name: 'Q') 
    q.technical_briefing(agent)
 
  # Exercise    
    favorite_gadget = agent.name
   
  # Verify
    expect(favorite_gadget).to eq 'James Bond'
 
  # Teardown is for now mostly handled by RSpec itself
  end

  it 'some clever description' do
    agent = Agent.create(name: 'James Bond', favorite_gadget: '007', favorite_gun: true)
   
    expect(agent).not_to be_bond
  end
 
end