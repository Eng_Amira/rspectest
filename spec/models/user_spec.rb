require 'rails_helper'

describe User  do
 

it 'transfer balance from user to another' do
  # Setup
  agent = User.create(name: 'amira', balance: '1000')
  user1 = User.where("id =?",1).first
  user2 = User.where("id =?",2).first
   
  # Exercise   
    x,y = agent.editbalance(1,2,50)
    balance1 = x
    balance2 = y
    
    #balance1 = agent.balance - 50
    #balance2 = agent2.balance + 50
   
  # Verify
    expect(balance1).to eq user1.balance - 50
    expect(balance2).to eq user2.balance + 50

 
  # Teardown is for now mostly handled by RSpec itself
  end
end
