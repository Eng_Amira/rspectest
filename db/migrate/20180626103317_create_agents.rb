class CreateAgents < ActiveRecord::Migration[5.1]
  def change
    create_table :agents do |t|
      t.string :name
      t.string :favorite_gadget
      t.string :favorite_gun

      t.timestamps
    end
  end
end
